#!/bin/sh

echo '----------'
# cat ./config.yml | tail -n +2
echo "DEFAULT_LANG: ${DEFAULT_LANG}"
echo "PAGE_PATH: ${PAGE_PATH}"
echo "VERSION_ALPINE: ${VERSION_ALPINE}"
echo "VERSION_PDFCPU: ${VERSION_PDFCPU}"
echo "VERSION_RUST: ${VERSION_RUST}"
echo "VERSION_WEASYPRINT: ${VERSION_WEASYPRINT}"
echo '----------'

sh ./z_scripts/qr_generate.sh || exit 1
sh ./z_scripts/compile_assets.sh || exit 1
sh ./z_scripts/optimise_pdfs.sh || exit 1

if [ "$DEV" = "1" ]; then
  echo 'Skipping compression'
  sh ./z_scripts/all_prettify.sh || exit 1
else
  sh ./z_scripts/compress_assets.sh || exit 1
fi

# main language is hardlinked onto index

for file in ./public/en.*; do
  # cp $file ${file/en/index}
  ln -f $file ${file/en/index}
done

sh ./z_scripts/pages_check.sh || exit 1
echo 'Done'
