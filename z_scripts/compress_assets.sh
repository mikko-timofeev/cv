#!/bin/sh

find public \
  -type f \( ! -iname "index.html" \) \
  -regex '.*\.\(htm\|html\|txt\|text\|js\|json\|css\|svg\|jpg\|webp\|wasm\)$' \
  -exec gzip -f -k {} \;
find public \
  -type f \( ! -iname "index.html" \) \
  -regex '.*\.\(htm\|html\|txt\|text\|js\|json\|css\|svg\|jpg\|webp\|wasm\)$' \
  -exec brotli -f -k {} \;
