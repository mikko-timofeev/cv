#!/bin/sh
# requires `git` for correct commit date retrieval
cargo build --release
###

wasm-pack build \
  --release \
  --no-typescript \
  --target web \
  --out-name helpers \
  --out-dir ./tmp/out \
    || exit 1

cp \
  ./tmp/out/helpers_bg.wasm \
  ./public/helpers_bg.wasm
cp \
  ./tmp/out/helpers.js \
  ./public/tmp/assets/scripts/helpers.js

tsc ./src/scripts/main.ts \
  --target esnext \
  --allowJs \
  --strict \
  --alwaysStrict \
  --removeComments \
  --outDir ./public/tmp/assets/scripts \
    || exit 1

js_scripts=$(ls ./public/tmp/assets/scripts/ | cut -f1 -d'.')
for scr in $js_scripts; do
  uglifyjs \
    "./public/tmp/assets/scripts/$scr.js" \
    --compress --mangle \
    -o "./public/$scr.js" \
      || exit 1
done

js_utils=$(ls ./utils/js/ | cut -f1 -d'.')
for scr in $js_utils; do
  uglifyjs \
    "./utils/js/$scr.js" \
    --compress --mangle \
    -o "./public/tmp/utils_min/js/$scr.js" \
      || exit 1
done

langs=$(ls ./src/locales/ | cut -f1 -d'.')
for lang in $langs; do
  date_change=$(git log -1 --date=format:"%Y-%m-%d" --format="%ad" "./src/locales/$lang.yml" 2> /dev/null || date -r "./src/locales/$lang.yml" "+%Y-%m-%d")
  # python3 ./libs/html_generator.py "$lang" "$langs" "$date_change" "${PAGE_PATH:-.}" \
  NODE_PATH=$(npm root -g) TEMPLATE_LANG=$lang DATE_CHANGE=$date_change ts-node "./src/views/index.ts" \
    | minify --type html \
    | tee "./public/$lang.html" \
      >/dev/null 2>&1 || exit 1

  # js-yaml "./src/locales/$lang.yml" \
  #   > "./public/tmp/assets/json/$lang.json" \
  #     || exit 1
  # json -f "./public/tmp/assets/json/$lang.json" \
  #   -e "$(cat ./public/tmp/utils_min/js/clean.js) clean(this)" \
  #   -o jsony-0 \
  #   > "./public/$lang.json" \
  #     || exit 1

  cat ./src/styles/*screen.scss \
    | sassc -s \
    | minify --type css \
    | tee ./public/screen.css \
      >/dev/null 2>&1 || exit 1
  sassc ./src/styles/common.scss \
    | minify --type css \
    | tee ./public/common.css \
      &> /dev/null || exit 1
  sassc ./src/styles/print.scss \
    | minify --type css \
    | tee ./public/print.css \
      >/dev/null 2>&1 || exit 1

  weasyprint "./public/$lang.html" "./public/tmp/documents/pdf/$lang-large.pdf" --optimize-images --jpeg-quality 100 \
    || exit 1

  printf "[ /Title (Mikhail Timofeev - CV)\
    \n  /Author (mikko-timofeev)\
    \n  /Subject (CV)\
    \n  /Keywords (CV, resume)\
    \n  /Creator (gitlab-ci)\
    \n  /DOCINFO pdfmark" \
      > "./public/tmp/documents/pdf/$lang-pdfmarks.txt"

  gs \
    -sDEVICE=pdfwrite \
    -dPDFSETTINGS=/screen \
    -dNOPAUSE \
    -dQUIET \
    -dBATCH \
    -dCompatibilityLevel=1.7 \
    -sOutputFile=./public/tmp/documents/pdf/$lang-unoptimised.pdf \
      "./public/tmp/documents/pdf/$lang-large.pdf" \
      "./public/tmp/documents/pdf/$lang-pdfmarks.txt" \
        || exit 1
done
