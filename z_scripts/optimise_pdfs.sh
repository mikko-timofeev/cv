#!/bin/sh

prefix='Mikhail_Timofeev_-_CV'

langs=$(ls ./src/locales/ | cut -f1 -d'.')
for lang in $langs; do
  if [ "$(command -v pdfcpu &> /dev/null && echo 1)" = "1" ]; then
    pdfcpu optimize \
      "./public/tmp/documents/pdf/$lang-unoptimised.pdf" \
      "./public/${prefix}_(${lang}).pdf" \
        || exit 1
  else
    cp \
      "./public/tmp/documents/pdf/$lang-unoptimised.pdf" \
      "./public/${prefix}_(${lang}).pdf" \
        || exit 1
  fi

  rm "./public/${lang}.pdf" 2> /dev/null || true
  # [ -L "./public/${lang}.pdf" ] || \
    # ln -s \
  # cp \
  ln -f \
    "./public/${prefix}_(${lang}).pdf" \
    "./public/${lang}.pdf"
done
