#!/bin/sh

langs=$(ls src/locales/ | cut -f1 -d'.')
for lang in $langs; do
  filename="./src/locales/$lang.yml"
  echo
  echo "$filename"
  pykwalify \
    -d "$filename" \
    -s ./src/locales_schema.yml \
    --strict-rule-validation \
      || exit 1
done
