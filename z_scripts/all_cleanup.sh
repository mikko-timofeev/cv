#!/bin/sh

sh ./z_scripts/cleanup_tmp.sh

yes | rm -f $(find public/. -maxdepth 1 -type f -name "*" ! -name "robots.txt") 2> /dev/null
find public \
  -regex '.*\.\(br\|gz\)$' \
  -exec rm {} \;
