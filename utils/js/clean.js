'use strict'
const clean = obj => {
  for (const key in obj) {
    const the_key = obj[key]
    let unset = false
    if (the_key == null || the_key === '') unset = true
    else {
      if (key[0] === '_') unset = true
      else {
        if (typeof the_key === 'object') obj[key] = clean(the_key)
        else if (the_key.endsWith('T00:00:00.000Z'))
          obj[key] = the_key.replace('T00:00:00.000Z', '')
      }
    }
    if (unset) obj[key] = undefined
  }
  return obj
}
