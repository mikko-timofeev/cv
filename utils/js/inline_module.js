// explicit imports
import init, { duration } from './helpers.js'

const run = async () => {
  await init()
}

run()

// easy calls through window object
Object.assign(globalThis, {
  duration: duration
})
