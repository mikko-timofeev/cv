// @ts-ignore
const os = require('fs')
// @ts-ignore
const template = require('pug')
// @ts-ignore
const yaml = require('js-yaml')
// @ts-ignore
const lang = process.env.TEMPLATE_LANG
// @ts-ignore
const dateChange = process.env.DATE_CHANGE

// Compile the source code
const templateExecutor = template.compileFile('./src/templates/index.pug')

const inlineModule = os.readFileSync(
  './public/tmp/utils_min/js/inline_module.js',
  'utf8'
)
const t = yaml.load(os.readFileSync(`./src/locales/${lang}.yml`, 'utf8'))
const tags = {}

// @ts-ignore
const extractTags = (section_id: string) => {
  t['data']['experiences']
    .find((section: { _id: string }) => section._id === section_id)
    ['items'].forEach((item: { [x: string]: string[] }) => {
      if ('summary' in item && 'dates' in item['summary']) {
        const dates = item['summary']['dates']
        const dateRange = [
          // @ts-ignore
          `${dates['start']}-01`,
          // @ts-ignore
          'end' in dates ? `${dates['end']}-28` : ''
        ].join(',')

        // @ts-ignore
        item['tags'].forEach((tag: string) => {
          if (tag in tags) {
            // @ts-ignore
            tags[tag].push(dateRange)
          } else {
            // @ts-ignore
            tags[tag] = [dateRange]
          }
        })
      }
    })
}

// extractTags('educated')
extractTags('experienced')

// Render a set of data
console.log(
  templateExecutor({
    dateChange: dateChange,
    inlineModule: inlineModule,
    lang: lang,
    t: t,
    tags: tags,
    // @ts-ignore
    pagePath: process.env.PAGE_PATH || '.',
    // @ts-ignore
    defaultLang: process.env.DEFAULT_LANG || 'en'
  })
)

// @ts-ignore
// console.log('<!-- ')
// console.debug(process.env.DEFAULT_LANG)
// console.log(' -->')
