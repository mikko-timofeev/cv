# Mikhail Timofeev

Are you reading this because you wanted to learn more about me?

|                                         |                                                                                                        |
| --------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| My CV can be viewed as                  | [the PDF](https://mikko-timofeev.gitlab.io/Mikhail_Timofeev_-_CV_(en).pdf) or [the webpage](https://mikko-timofeev.gitlab.io/) |
| Find my organised project library in    | [the Groups](https://gitlab.com/users/mikko-timofeev/groups)                                           |
| Some of my experiments are listed among | [the Snippets](https://gitlab.com/users/mikko-timofeev/snippets)                                       |
