#!/bin/sh
# Requirements:
#  aardvark-dns
#  netavark
#  podman
#  podman-compose
#  python
#  yq
#
# 0. ./run.sh prepare
# 1. ./run.sh build
# 2. ./run.sh start
# 3. ./run.sh compile
# 4. ./run.sh console
#      ./all_verify.sh
#      ./all_cleanup.sh
#      ./all_compile.sh
# 5. ./run.sh stop

export VERSION_ALPINE=$(yq -r .variables.VERSION_ALPINE ./config.yml)
export VERSION_PDFCPU=$(yq -r .variables.VERSION_PDFCPU ./config.yml)
export VERSION_RUST=$(yq -r .variables.VERSION_RUST ./config.yml)
export VERSION_WEASYPRINT=$(yq -r .variables.VERSION_WEASYPRINT ./config.yml)

export _UID=$(id -u)
export _GID=$(id -g)
export _UNAME=$(id -un)
export _GNAME=$(id -gn)

export DEFAULT_LANG=${DEFAULT_LANG:-en}
export PAGE_PATH=${PAGE_PATH:-localhost}

# composer='docker compose'
composer='podman-compose'

if [ "$1" = 'build' ]; then
    $composer build \
        --build-arg _UID=${_UID} \
        --build-arg _GID=${_GID} \
        --build-arg _UNAME=${_UNAME} \
        --build-arg _GNAME=${_GNAME} \
        \
        --build-arg VERSION_ALPINE=${VERSION_ALPINE} \
        --build-arg VERSION_PDFCPU=${VERSION_PDFCPU} \
        --build-arg VERSION_RUST=${VERSION_RUST} \
        --build-arg VERSION_WEASYPRINT=${VERSION_WEASYPRINT}
elif [ "$1" = 'start' ]; then
    $composer down
    echo "Starting http://mikko-timofeev.localhost"
    _UID=${_UID} \
    _GID=${_GID} \
    _UNAME=${_UNAME} \
    _GNAME=${_GNAME} \
        $composer up -d
elif [ "$1" = 'stop' ]; then
    $composer down
elif [ "$1" = 'compile' ]; then
    $composer exec web bash -c "./z_scripts/all_compile.sh"
elif [ "$1" = 'console' ]; then
    $composer exec web bash
elif [ "$1" = 'serve' ]; then
    sudo python -m http.server -d ./public 80
elif [ "$1" = 'prepare' ]; then
    sudo su -c "\
        yes | pacman -S aardvark-dns netavark yq; \
        rm ./tmp/out/*; \
        chmod -R a+w ./target/* ./tmp/* ./public/*; \
        cp ./etc_containers_registries.conf /etc/containers/registries.conf \
    "
else
    printf 'Available options:\n  build\n  start\n  stop\n  compile\n  console\n  serve\n  prepare\n\n'
fi
